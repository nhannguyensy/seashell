#!/usr/bin/env bash
#
# Docker build script
# Rebuild with

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

DOCKERHUBUSER=pivert
IMAGEBUILDNAME=seashell
# docker build -t pivert/$IMAGEBUILDNAME:$(git tag | tail -n1) -t pivert/$IMAGEBUILDNAME:latest --build-arg GIT_COMMIT=$(git log -1 --format=%h) --build-arg GIT_TAG=$(git tag | tail -n1) .

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat << EOF # remove the space between << and EOF, this is due to web plugin issue
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

Script description here.

Available options:

-h, --help             Print this help and exit
-v, --verbose          Verbose
-l, --build-locally    Build the image locally
-r, --build-remote     Build the image remotely (Gitlab)
-p, --push             Push the latest locally build image to dockerhub
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  build_locally=''
  push=0

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -p | --push) push=1 ;;
    -l | --build-locally) build_locally='local' ;;
    -r | --build-remote) build_locally='remote' ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  # [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments (check with --help)"
  [[ -z "${build_locally-}" ]] && (( ${push} == 0 ))\
    && die "Missing required parameter: param (check with --help)"

  return 0
}

parse_params "$@"
setup_colors

# script logic here

check_nothing_to_commit() {
  CURBRANCH=$(git branch --show-current)

  if [[ $(git branch --show-current) != 'main' ]]
  then
    NEWTAG="${CURBRANCH}"
    return 0
  fi


  if git status | grep "nothing to commit"
  then
    TAG=$(git tag | tail -n1)
    NEWTAG=$(bc <<< "$TAG + 0.01")
    # sed -E -i -e "s/^(LABEL org.opencontainers.image.version=).*/\1\"${NEWTAG}\"/" Dockerfile
    git commit -am "Updated image version to ${NEWTAG}" || true
    git push || echo "git push exit 1"
    echo "Tagging with $NEWTAG"
    git tag $NEWTAG
    git push origin $NEWTAG

  else
    echo "ERROR: Work to be commited - Diff:"
    git diff
    echo "ERROR: Work to be commited - Commit first"
    exit 1

  fi
}

push_image() {
  PUSHTAG=$1
  docker push "${DOCKERHUBUSER}/${IMAGEBUILDNAME}:latest"
  docker push "${DOCKERHUBUSER}/${IMAGEBUILDNAME}:${PUSHTAG}"
}

TAG=$(git tag | tail -n1)

case "${build_locally}" in 
  local)
    echo "Building locally"
    check_nothing_to_commit
    echo "Starting docker build"

    # Build locally
    CMD="""
    docker build --no-cache
    -t "${DOCKERHUBUSER}/${IMAGEBUILDNAME}:${NEWTAG}"
    -t "${DOCKERHUBUSER}/${IMAGEBUILDNAME}:latest"
    --build-arg GIT_COMMIT=$(git log -1 --format=%h)
    --build-arg GIT_TAG=${NEWTAG}
    .
    """

    (echo $CMD && $CMD) 2>&1 | tee >(xz -z > "${DOCKERHUBUSER}_${IMAGEBUILDNAME}_${NEWTAG}.log.xz")

    if (( push == 1 ))
    then
      echo "Pushing docker image"
      push_image ${NEWTAG}
    fi
  ;;

  remote)
    check_nothing_to_commit
    echo "Building remote"
    if git status | grep "nothing to commit"
    then
      NEWTAG=$(bc <<< "$TAG + 0.01")
      sed -E -i -e "s/^(LABEL org.opencontainers.image.version=).*/\1\"${NEWTAG}\"/" Dockerfile
      git commit -am "Updated image version to ${NEWTAG}"
      git push
      git tag $NEWTAG
      git push --tags
    else
      echo "ERROR: Work to be commited"
      git diff
      exit 1
    fi
  ;;

  '')
    echo "No Rebuild"
    if (( push == 1 ))
    then
      push_image ${TAG}
    fi
  ;;
esac

