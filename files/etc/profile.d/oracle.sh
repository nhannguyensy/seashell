# Oracle Configuration
export TNS_ADMIN=~/.config/oracle/tns_admin

export ORACLE_HOME=/opt/oracle/instantclient
export LD_LIBRARY_PATH="$ORACLE_HOME"

export PATH=/user/.local/bin:$ORACLE_HOME:$PATH
export TNS_ADMIN=/user/oracle/network/admin

