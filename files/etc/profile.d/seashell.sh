######################################################################
# python-nvim-ide additional config                                  #
######################################################################

# Default to vim mode
set -o vi
# But keep some readline common shortcuts
# https://www.reddit.com/r/bash/comments/r38oi/if_you_use_vimode_under_bash_and_you_miss_escdot/
bind -m vi-command ".":insert-last-argument
bind -m vi-insert "\C-l.":clear-screen
bind -m vi-insert "\C-a.":beginning-of-line
bind -m vi-insert "\C-e.":end-of-line
bind -m vi-insert "\C-w.":backward-kill-word
bind -m vi-insert "\C-k":kill-line
bind -x '"\C-l": clear;'


export LS_OPTIONS='--color=auto'
eval "$(dircolors -b)"
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
alias gcam='git commit -am'
alias gp='git push'
alias gco='git checkout'
alias gb='git branch'
alias ssha='$(ssh-agent -s) && ssh-add'
 
# Some more aliases to avoid making mistakes:
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Ensure 256 colors
export TERM=screen-256color

# Console colors
if tput setaf 1 &> /dev/null; then
    tput sgr0
    if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
      BASE03=$(tput setaf 234)
      BASE02=$(tput setaf 235)
      BASE01=$(tput setaf 240)
      BASE00=$(tput setaf 241)
      BASE0=$(tput setaf 244)
      BASE1=$(tput setaf 245)
      BASE2=$(tput setaf 254)
      BASE3=$(tput setaf 230)
      YELLOW=$(tput setaf 136)
      ORANGE=$(tput setaf 166)
      RED=$(tput setaf 160)
      MAGENTA=$(tput setaf 125)
      VIOLET=$(tput setaf 61)
      BLUE=$(tput setaf 33)
      CYAN=$(tput setaf 37)
      GREEN=$(tput setaf 64)
    else
      BASE03=$(tput setaf 8)
      BASE02=$(tput setaf 0)
      BASE01=$(tput setaf 10)
      BASE00=$(tput setaf 11)
      BASE0=$(tput setaf 12)
      BASE1=$(tput setaf 14)
      BASE2=$(tput setaf 7)
      BASE3=$(tput setaf 15)
      YELLOW=$(tput setaf 3)
      ORANGE=$(tput setaf 9)
      RED=$(tput setaf 1)
      MAGENTA=$(tput setaf 5)
      VIOLET=$(tput setaf 13)
      BLUE=$(tput setaf 4)
      CYAN=$(tput setaf 6)
      GREEN=$(tput setaf 2)
    fi
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
else
    # Linux console colors. Not sure about Solarized values.
    MAGENTA="\033[1;31m"
    ORANGE="\033[1;33m"
    GREEN="\033[1;32m"
    PURPLE="\033[1;35m"
    WHITE="\033[1;37m"
    BOLD=""
    RESET="\033[m"
fi

export KUBECONFIG=~/.kube/config
__ctx='`/usr/local/bin/get_ctx_ns $KUBECONFIG`'

__git_branch='`git branch 2> /dev/null | grep -e ^* | sed -E  s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /`'
# PS1="\[$BASE01\]\t\[$BLUE\][\h]\[$GREEN\][\u] \[$VIOLET\]$__git_branch\[$CYAN\]\w\n\[$GREEN\]\$\[$RESET\]"
PS1="\[$BASE01\]\t\[$BLUE\][\h]\[$GREEN\][\u]\[$VIOLET\]$__git_branch\[$ORANGE\]$__ctx \[$CYAN\]\w\n\[$GREEN\]\$\[$RESET\]"
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'
alias tmux='tmux -u -2'

# Nvim
alias view='nvim -R'
alias ex='nvim -e'
alias vimdiff='nvim -d'

# Easy way to go back to workdir
alias cdwd='cd /workdir'

# Easy way to see the relevant configuration of the container
alias config='env | nvim -R \
    /README.md /init.sh /etc/profile.d/python-nvim-ide.sh $HOME/.config/nvim/init.vim -'

# Ensure PATH are defined if needed
export PATH="$HOME/.krew/bin:$HOME/bin:$HOME/.local/bin:$PATH"

# Configure git with env
if [[ ! -e $HOME/.gitconfig ]]
then 
  if [[ -f $HOME/.gitconfighost ]]
  then
    cp -f $HOME/.gitconfighost $HOME/.gitconfig
  else
    [ -n "${GIT_EMAIL}" ] && git config --global user.email "${GIT_EMAIL}"
    [ -n "${GIT_NAME}" ] && git config --global user.name "${GIT_NAME}"
    git config --global merge.tool vimdiff
    git config --global merge.conflictstyle diff3
    git config --global mergetool.prompt false
  fi
fi 


# Configure ssh pubkey if provided
# Or mount your .ssh folder : 
# -v ~/.ssh:$HOME/.ssh
[ -n "${RSA_PUBKEY}" ] && echo "${RSA_PUBKEY}" >> $HOME/.ssh/id_rsa.pub

# Change the terminal title
echo -ne "\033]30;$(hostname)\007"

cat /etc/issue
cat /etc/motd

cd /workdir

if [ -n "$BASH_VERSION" -a -n "$PS1" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi

krestart() {
  for type in StatefulSet Deployment
  do
    kgojsonn $(kns -c) $type | jq -r '.items[] | "\n# Copy/Paste to restart \(.metadata.name) '${type}' Pod\nk scale -n '$(kns -c)' '${type}' \(.metadata.name) --replicas 0\nk scale -n '$(kns -c)' '${type}' \(.metadata.name) --replicas \(.spec.replicas)" '
  done
}

alias nocolors="sed 's/\x1b[[0-9;]*m//g'"
