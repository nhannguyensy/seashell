# Keep history for the current user only
export HISTFILE=/workdir/.bash_history_seashell

# Aliases & completion
source /usr/local/bin/complete_alias

for al in $(sed -En 's/^alias ([^=]+)=.*/\1/p' /etc/profile.d/aliases_kubectl.sh)
do complete -F _complete_alias "${al}"; done;

# aws-cli
complete -C '/usr/local/bin/aws_completer' aws
# Also completion for the a alias
alias a='aws'
complete -C '/usr/local/bin/aws_completer' a

# mkdir & cd
alias mkdircd='function _mkdircd() { mkdir -p "$1" && cd "$1"; }; _mkdircd'

# Completion for flux
if [[ -x /usr/local/bin/flux ]]; then
    . <(/usr/local/bin/flux completion bash)
fi

# k9s
. <(k9s completion bash)

# k
# . <(k completion bash)
