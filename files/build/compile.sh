#!/bin/bash
#
set -Eeuo pipefail

# This script installs software compiled from source
# Dependances for Neovim 
apt-get install -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config doxygen build-essential libffi-dev pkg-config

# Build and install NeoVIM
git clone -b stable --single-branch --depth 1 https://github.com/neovim/neovim.git
cd neovim
make CMAKE_BUILD_TYPE=Release
make install

# Clean NeoVIM sources
cd ..
rm -rf neovim

# Set Neovim shortcuts
BIN_PATH=/usr/local/bin
update-alternatives --install /usr/bin/vi neovim "${BIN_PATH}/nvim" 110
update-alternatives --set neovim "${BIN_PATH}/nvim"
update-alternatives --install /usr/bin/vim vim "${BIN_PATH}/nvim" 110
update-alternatives --install /usr/bin/editor editor "${BIN_PATH}/nvim" 1

mkdir tmp && cd tmp/
# Install kubectl
KUBECTL_VERSION=$(curl -L -s https://dl.k8s.io/release/stable.txt)
echo "Installing kubectl client version $KUBECTL_VERSION"
curl -LO "https://dl.k8s.io/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$KUBECTL_VERSION/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Install helm (The package manager for Kubernetes)
curl -sLS https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
helm completion bash > /etc/bash_completion.d/helm

# Syft for building SBoM from docker images
curl -sSfL https://raw.githubusercontent.com/anchore/syft/main/install.sh | sh -s -- -b /usr/local/bin

cd ..
rm -rf tmp/

# Install custom get_ctx_ns
gcc -O3 get_ctx_ns.c -o /usr/local/bin/get_ctx_ns
chmod a+x /usr/local/bin/get_ctx_ns

# Install OpenFaaS (requires compilation)
# curl -sSL https://cli.openfaas.com | sh
# faas-cli completion --shell bash > /etc/bash_completion.d/faas-cli

# Install PostgreSQL python bindings
pip install psycopg2

# Clean up build packages
apt-get purge -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config doxygen  build-essential libffi-dev pkg-config

apt-get autoremove -y
apt-get clean

rm -rf /root/.cache

