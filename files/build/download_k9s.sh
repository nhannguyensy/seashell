#!/usr/bin/env bash
# Download k9s precompiled and verify the checksum

set -Eeuo pipefail

mkdir k9s
cd k9s

K9S_CKFILE="checksums.sha256"
K9S_VERSION=$(curl -L https://api.github.com/repos/derailed/k9s/releases/latest | jq .name -r)
K9S_CS_URL="https://github.com/derailed/k9s/releases/download/${K9S_VERSION}/${K9S_CKFILE}"
K9S_URL="https://github.com/derailed/k9s/releases/download/${K9S_VERSION}/k9s_Linux_amd64.tar.gz"


echo "Download: K9S_URL=${K9S_URL}"
curl -sSfL "${K9S_URL}" -O
echo "Download: K9S_CS_URL=${K9S_CS_URL}"
curl -sSfL "${K9S_CS_URL}" -O

if sha256sum -c ${K9S_CKFILE} --ignore-missing
then
  tar -zxvf k9s_Linux_amd64.tar.gz -C /usr/local/bin/ k9s
else
  echo "ERROR: Checksum verification failed"
  exit 1
fi

cd ..
rm -rf k9s
