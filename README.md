# seashell - K8S, Python & Neovim centric DevOps toolbox

Kubernetes, Python and Vim centric development & runner environment for DevOps engineers.

Enjoy a pre-configured DevOps toolbox in Docker including hundred of tools. `vim /sbom.txt.gz` for the list. Few of them:
- K8S & Cloud: kubectl, k9s, docker, helm, yq, git-crypt, [800 easy-to-remember kubectl aliases from Ahmet](https://ahmet.im/blog/kubectl-aliases/) with bash completion (`<ctrl>j` to expand aliases)
- Neovim plugins via Plug or Coc language servers for formatting, linting and development.
- Preconfigured Bash `<tab>` completion for most tools
- Bash `set -o vi` by default, but with most common Emacs shortcuts still working.
- Latest stable versions with a consistent sober Solarized or Molokai dark theme for Bash and Neovim
- Optimized image of only 800MB compressed, built to be used in offline environments.
- Helper installation scripts in ~/bin for commands like aws-cli, SAM (Lambda), dive, flux, pwsh, sgpt, sqlcmd, sqlplus, vue... (requires internet)

# How to start
Seashell script is build to run locally (not on K8S).
- docker required (i.e. you're member of the docker group)
- cd to a folder in your $PATH, for instance in /usr/local/bin as root, or in your $HOME/bin
- Download the startup script:

    ```
    curl -O https://gitlab.com/pivert/seashell/-/raw/main/seashell && chmod a+x seashell
    ```

- Run it:

    ```
    ./seashell                # To get a shell with tmux, neovim, python, pyright, ansible, ...
          OR
    ./seashell vim file.py    # To start editing directly in the environment. Template will be used if file.py does not exist.
          OR
    ./seashell -u             # Check & update to latest docker image before starting
    ```
    ```
    ./seashell -r to get a second shell in the same Docker container
    ```

- Install & run in one line:
    ```
    SS=/usr/local/bin/seashell; sudo curl https://gitlab.com/pivert/seashell/-/raw/main/seashell -o $SS && sudo chmod a+x $SS && $SS
    ```

- Alternatively, you can use a more advanced function to keep it up to date with a download every 10 days. Add this at the end of your .bashrc and make sure ~/bin is in your path. (Bash should add ~/bin to your path at the next login when ~/bin exists)

```
seashell() {
  mkdir -p ~/bin
  SS=~/bin/seashell
  URL=https://gitlab.com/pivert/seashell/-/raw/main/seashell
  
  if [ -f "${SS}" ] && [ $(find "$SS" -mtime +10) ]; then
    mv "$SS" "${SS}.old"
  fi

  if [ ! -f "$SS" ]; then
    curl -s -m 3 "$URL" -o "$SS" && chmod +x "$SS"
  fi

  if [ -f "${SS}.old" ] && [ ! -f "${SS}" ]; then
    cp "${SS}.old" "${SS}"
  fi
  "$SS" "$@"
}
```

- The seashell script will detect and keep your user id and try to preserve most of your local environment. Read the `seashell` startup script to understand what it passes to the Seashell container. i.e.

    ```
    $grep volumes seashell
    # The below volumes will be mounted if they exists
    declare -a volumes
    volumes+=("/etc/passwd:/etc/passwdhost:ro")
    volumes+=("/etc/group:/etc/grouphost:ro")
    volumes+=("/etc/ansible/hosts:/etc/ansible/hosts")
    # volumes+=("/dev/ttyUSB0:/dev/ttyUSB0") # For micropython programming
    volumes+=("${HOME}/.ssh:/user/.ssh")
    volumes+=("${HOME}/.gnupg:/user/.gnupg")
    volumes+=("${HOME}/.aws:/user/.aws")
    volumes+=("${HOME}/.kube:/user/.kube")
    volumes+=("${HOME}/.docker:/user/.docker")
    volumes+=("${HOME}/.config/helm:/user/.config/helm")
    volumes+=("${PWD}:/workdir")
          volumes+=("${2-}")
    volumes+=("${GITCONFIG}:/user/.gitconfighost:ro")
    for vol in "${volumes[@]}"
    ```

# Colors & Fonts
- Use Powerline / glyphs patched fonts such as [Nerd Fonts](https://www.nerdfonts.com/). Start with Hack font.
- Solarized Dark color scheme.

Screenshot of what you see after starting seashell then vim a new empty Dockerfile (it loads a template if file does not exist)
![Neovim screenshot](https://gitlab.com/pivert/seashell/-/raw/main/screenshots/nvim1.webp "nvim ./Dockerfile (template)")


## Command details

# Features:
- Syntax for Python, Ansible, Dockerfile, vimrc, YAML & JSON
- Latest Ansible & Collection (Run /untaransiblecollection.sh before using Ansible Collection)
- Other tools git tmux nodejs(latest) npm yarn curl wget htop rsync nc nmap unzip xz ...
- Latest neovim with plugins:
  - Python, Dockerfile & Bash basic templates for new scripts
  - neoclide/coc.nvim intellisense engine with several plugins:
    - [coc-pyright](https://github.com/fannheyward/coc-pyright)
    - [coc-spell-checker](https://github.com/iamcco/coc-spell-checker)
    - [coc-yaml](https://github.com/neoclide/coc-yaml)
    - [coc-json](https://github.com/neoclide/coc-json)
    - [coc-git](https://github.com/neoclide/coc-git)
    - [coc-prettier](https://github.com/neoclide/coc-prettier)
    - [coc-docker](https://github.com/josa42/coc-docker)
    - [coc-ansible](https://github.com/yaegassy/coc-ansible)
  - arouene/vim-ansible-vault         [Ansible vault or un-vault values](https://github.com/arouene/vim-ansible-vault)
  - kien/ctrlp.vim                    [Fuzzy finder for vim](https://github.com/kien/ctrlp.vim)
  - scrooloose/nerdcommenter          [Comment/Uncomment tool](https://github.com/preservim/nerdcommenter#usage)
  - scrooloose/nerdtree               A Tree-like side bar for better navigation
  - tmhedberg/matchit                 Switch to the beginning and the end of a block by pressing %
  - vim-airline/vim-airline           A cool status bar
  - vim-airline/vim-airline-themes    Airline themes
  - altercation/vim-colors-solarized  Solarized colorscheme (default to dark theme)
  - arcticicestudio/nord-vim          Nord
  - sheerun/vim-polyglot              Better syntax-highlighting for filetypes in vim
  - tpope/vim-fugitive                Git integration
  - tpope/vim-surround                Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML tags, and more.
  - jiangmiao/auto-pairs              Auto-close braces and scopes
  - jpalardy/vim-slime', { 'for': 'python' } 
  - tmhedberg/SimplyFold              Python code folding - zo zO zc zC
- Solarized terminal colors for:
  - tmux
  - bash
  - neovim
- Other tools
  - AWS Serverless Application Model tools: aws-cli and sam (installation script)
  - vue (installation script)
  - Most command-line tools such as git, rsync, nc, nmap, unzip, xz,...


## Neovim
- [Neovim Documentation](https://neovim.io/doc/general/)
- use :checkhealth to get status of the editor setup
- ]g and [g go to next/previous coc diagnostic error/warning
- K on keywords to get doc, C-w + w  to go in doc buffer/floating window, :q to go back to main buffer
- gr/gd : Go Reference, Go Definition
- Basic buffer commands (read help for more):
  - :bn[ext]/:bp[revious]/:bf[irst]/:bl[ast]/:b2/:bc[lose] : go to buffer next/previous/first/last/second/close current buffer
  - :e[dit] newfile.py : Create a new buffer, add it to the list, and open the newfile.py in it
- Check installed Plug plugins : PlugStatus
- Check installed Coc extensions : CocList extensions

## Quick tips
- tmux (C-b instead of screen C-a ): C-b-% C-b-" C-o C-b-,
- Neovim
    - vim : :checkhealth :CocList :CocInfo
    - vim-slime + tmux + ipython : C-c C-c from nvim
    - [vim-surround](https://github.com/tpope/vim-surround): cs'" / S" (visual) / ysiw(
    - vim Leader key is Space key: Space-r-n (rename symbol)
    - Try :CocAction :CocCommands :verbose
    - vim-fold: za/zA zo/zO zc/zC zm/zr
    - [nerdcommenter](https://github.com/preservim/nerdcommenter): Space-cc Space-cu
    - [suda.nvim](https://github.com/lambdalisue/suda.vim) : Uses sudo when file not writeable
    - <Space>a: CocAction, will propose action on misspelled words or other tips
- Bash
    - Many aliases with autocompletion:
        - k<TAB> for all 800 kubectl aliases [Check documentation](https://ahmet.im/blog/kubectl-aliases/). Just take the **bold** characters from the table below to call the alias. For instance: `kubectl --namespace=kube-system get secret -o=json` can be called with alias `ksysgsecojson`

        | base    | [system]   | commands           | resources            | [flags]               | [value flags]    |
        |---------|------------|:------------------:|:--------------------:|-----------------------|------------------|
        |         |            | <span style="color:green">**g**</span>et            | <span style="color:green">**po**</span>d              | <span style="color:green">**oyaml**</span>             |                  |
        |         |            | <span style="color:green">**d**</span>escribe       | <span style="color:green">**dep**</span>loyment       | <span style="color:green">**ojson**</span>             |                  |
        |         | -n=kube-   | <span style="color:green">**rm**</span> = delete    | <span style="color:green">**ing**</span>gress         | <span style="color:green">**owide**</span>             | --<span style="color:green">**n**</span>amespace  |
        |  <span style="color:green">**k**</span>  | <span style="color:green">**sys**</span>tem | <span style="color:green">**a**</span>pply -f       | <span style="color:green">**s**</span>er<span style="color:green">**v**</span>i<span style="color:green">**c**</span>e  | --<span style="color:green">**all**</span>[-namespace] | --<span style="color:green">**f**</span>ilename   |
        |         |            | <span style="color:green">**a**</span>pply -<span style="color:green">**k**</span>   | <span style="color:green">**c**</span>onfig<span style="color:green">**m**</span>ap    | --<span style="color:green">**s**</span>how-<span style="color:green">**l**</span>abels | --se<span style="color:green">**l**</span>ector   |
        |         |            | <span style="color:green">**k**</span>ustomize      | <span style="color:green">**sec**</span>ret           | --<span style="color:green">**w**</span>atch           |                  |
        |         |            | <span style="color:green">**ex**</span>ec -i -t     | <span style="color:green">**n**</span>ame<span style="color:green">**s**</span>pace    |                       |                  |
        |         |            | <span style="color:green">**lo**</span>gs -f        | <span style="color:green">**no**</span>de             |                       |                  |

        - a for aws-cli
        - faas for [faas-cli](https://github.com/openfaas/faas-cli)
    - <CTRL>+j expand any alias
    - [Solarized](https://ethanschoonover.com/solarized/) dark colors (like in nvim)
    - alias gcam='git commit -am'
    - alias cdwd='cd /workdir/'
- [kubectx](https://github.com/ahmetb/kubectx) (with bash completion) 
    - kubens to change active namespace
    - kubectx to change context/cluster 

## SSH
You might want to use ssh with pubkey authentication from the container.
You might also want to cache your ssh key password.
The first thing is to mount your .ssh folder when running the container.
Add -v ~/.ssh:/user/.ssh 

Then you have 2 options:
- run 'eval $(ssh-agent) > /dev/null' then 'ssh-add' in the container.
- run ssh-add before running the container, then reuse the agent by adding 
  '-f $SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent' to your docker run command

## Customization
Most customization can be done by mounting updated config file and/or 
adapting the seashell local shell script.

You can place an executable init script in the root of the folder named
docker-environment-root.sh (executed as root before the su -l)
This feature might be removed in the future.

## Further Help
Source code & issues: https://gitlab.com/pivert/seashell/ 
Check online documentation from tmux, nvim, and the plugin you would like to use.
Run 'config' alias from bash in the container.
Also check SLIME and ipython-cell plugin for Tmux & ipython integrations.

## Security
Security limitations from docker
Built to be used in offline environments without internet access

## Micropython
Check /build/requirements.txt to see installed stubs, and install new ones if needed.
Micropython is not part of the image. To install it:

```
sudo apt-get update && apt-get install -y gcc libffi --no-install-recommends
curl -O https://micropython.org/resources/source/micropython-1.19.1.tar.xz
tar -Jxvf micropython-1.19.1.tar.xz 
cd micropython-1.19.1/
cd ports/unix
make submodules
make
```

# Limitations
- Solarized dark theme & Patched font dependencies on your local terminal. Use another colorscheme in vim if it's a problem.

# Tips
## AWS SAM
`sam invoke local` command requires special arguments to succeed, since it's not running local to the docker daemon :
```
sam local invoke \
  -v /<absolute path to SAM project on host>/.aws-sam/build \
  --container-host $(docker network inspect bridge | jq -r '.[0].IPAM.Config[0].Gateway') \
  --container-host-interface 0.0.0.0 \
  --debug
```

# Seek for help
- I'm interested in any help to further shrink the image or to improve UX.
